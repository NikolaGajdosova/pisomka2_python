def eval_expr(c,d={}):

    g=[]
    c1=list(c.split(" "))
    
    for a in c1:
        
        if a.isdigit() == True:
            g.append(int(a))
            
        elif a in d:
            g.append(d[a])
            
        elif a == '+':
            a=g.pop()
            b=g.pop()
            g.append(a+b)
        elif a == '-':
            a=g.pop()
            b=g.pop()
            g.append(b-a)
        elif a == '*':
            a=g.pop()
            b=g.pop()
            g.append(a*b)
        elif a == '/':
            a=g.pop()
            b=g.pop()
            g.append(b/a)

    return g[0]

def to_infix(c):

    g=[]
    d="( "
    c1=list(c.split(" "))
    
    for i in c1:
        
        if i.isdigit() == True:
            g.append(i)
            
        else:
            if len(g) > 1:
                a=g.pop()
                b=g.pop()
                d=d+'( '+b+' '+i+' '+a+' )'
                t='( '+b+' '+i+' '+a+' )'
            else:
                a=g.pop()
                d=d+' '+i+' '+a+' )'
                
    
    return d
